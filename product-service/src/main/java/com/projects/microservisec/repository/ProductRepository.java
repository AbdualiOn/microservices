package com.projects.microservisec.repository;

import com.projects.microservisec.model.Product;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductRepository extends MongoRepository<Product, String>{
}
